# ArmAsm

Quick assembler for LEGv8.

![ArmAsm Screenshot](https://bitbucket.org/Gliderman/armasm/raw/f6c374eb5ef0e6bbbec431fe350860143a4e080f/ArmAsm.PNG)

## Download

Download a ready-to-run version from [Downloads](https://bitbucket.org/Gliderman/armasm/downloads/). It should run on Java 6 and up.
