package org.coinofgold.armasm;

public class Instruction {

	public String inst;
	public String binary;
	public int numParts;
	
	public Instruction(String inst, String binary, int numParts) {
		this.inst = inst;
		this.binary = binary;
		this.numParts = numParts;
	}
	
}
