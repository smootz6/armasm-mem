package org.coinofgold.armasm;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.*;

public class UI extends JFrame {
	
	static final String NOP = new String(new char[32]).replace("\0", "0");

	// Basic layout
	JPanel mainPanel = new JPanel();
		JPanel southPanel = new JPanel();
			JButton saveToFileButton = new JButton("Save To File");
			JComboBox textFormatChooser = new JComboBox(new String[]{
					"Raw Instructions Format",
					"Rom Case Format",
					".mem File Format"});
			JCheckBox hexModeCheckBox = new JCheckBox("Hex Mode");
			JCheckBox reverseInputOutputOrder = new JCheckBox("Reverse In/Outs");
			JLabel fileNameLabel = new JLabel(" File Name:");
			JTextField fileNameField = new JTextField("ROM", 10);
			JLabel betaLabel = new JLabel("Designed and built by Nate Hoffman.");
		JSplitPane centerPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
				JTextArea leftTextArea = new JTextArea();
			JScrollPane leftScroll = new JScrollPane(leftTextArea);
				JTextArea rightTextArea = new JTextArea();
			JScrollPane rightScroll = new JScrollPane(rightTextArea);
	
	public UI() {
		// Window title
		super("ArmAsm");
		// Build the window for use
		buildWindow();
	}
	
	public void buildWindow() {
		// Listen for typing
		leftTextArea.getDocument().addDocumentListener(new CodeDocumentListener());
		// Stop the user from editing the generated code
		rightTextArea.setEditable(false);
		// Make the font bigger
		Font f = leftTextArea.getFont();
		leftTextArea.setFont(f.deriveFont(18f));
		rightTextArea.setFont(f.deriveFont(18f));
		
		// Saves output to file
		saveToFileButton.addActionListener(new genFileActionListener());
		// Changes output format
		textFormatChooser.addActionListener(new ReassembleActionListener());
		// Checkboxes
		hexModeCheckBox.setEnabled(false);
		reverseInputOutputOrder.addActionListener(new ReassembleActionListener());
		fileNameField.addActionListener(new ReassembleActionListener());
		fileNameField.getDocument().addDocumentListener(new CodeDocumentListener());

		southPanel.add(textFormatChooser);
		southPanel.add(hexModeCheckBox);
		southPanel.add(reverseInputOutputOrder);
		southPanel.add(fileNameLabel);
		southPanel.add(fileNameField);
		southPanel.add(saveToFileButton);
		southPanel.add(betaLabel);
		
		// 1x2 grid with the two text areas
		centerPane.add(leftScroll);
		centerPane.add(rightScroll);
		
		// Add the panels into a border layout
		mainPanel.setLayout(new BorderLayout());
		mainPanel.add(centerPane, BorderLayout.CENTER);
		mainPanel.add(southPanel, BorderLayout.SOUTH);
		
		// Close as soon as the user closes the window
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Add the panel to the frame
		this.add(mainPanel);
		
		// Resize
		this.pack();
		this.setSize(1000, 600);
		centerPane.setDividerLocation(490);
		reverseInputOutputOrder.setEnabled(false);
		rightTextArea.setText(genLeftText());
	}
	
	public String genLeftText() {
		// Read the text
		String codeText = leftTextArea.getText();
		// Split up the lines
		String[] codeArray = codeText.split("\n");
		// A location to place the assembled program
		StringBuilder sb = new StringBuilder();
		
		// Rom case
		if (textFormatChooser.getSelectedIndex() == 1) {
			if (reverseInputOutputOrder.isSelected()) {
				sb.append("module ").append(fileNameField.getText()).append("(out, address);\n");
			} else {
				sb.append("module ").append(fileNameField.getText()).append("(address, out);\n");
			}
			sb.append("   input[15:0] address;\n");
			sb.append("   output reg[31:0] out;\n");
			sb.append("   always @(address) begin\n");
			sb.append("      case(address)\n");
		}
		
		// Scan through to find any labels and store their locations
		Assembler.resetLabels();
		int scanOffset = 0;
		for (int i = 0; i < codeArray.length; i++) {
			// Get the string
			String codeLine = codeArray[i];
			
			if (codeLine.trim().startsWith("//")) {
				// Ignore
				scanOffset++;
			} else {
				// Break up the line around any //
				String[] codeLineParts = codeLine.trim().split("//");
				
				// Call the assembler
				String assembled = Assembler.assemble(codeLineParts[0], i - scanOffset, true);
				
				// Display if there is a value
				if (assembled != null) {
					if (assembled.equals("LABEL")) {
						scanOffset++;
					} else {
						sb.append(assembled).append("\n");
					}
				}
			}
		}
		
		// Assemble the lines one at a time
		int asmOffset = 0;
		for (int i = 0; i < codeArray.length; i++) {
			// Get the string
			String codeLine = codeArray[i];
			
			if (codeLine.trim().startsWith("//")) {
				if (textFormatChooser.getSelectedIndex() == 1) {
					sb.append("         ").append(codeLine).append("\n");
				} else {
					sb.append(codeLine).append("\n");
				}
				asmOffset++;
			} else {
				// Break up the line around any //
				String[] codeLineParts = codeLine.trim().split("//");
				
				// Call the assembler
				String assembled = Assembler.assemble(codeLineParts[0], i - asmOffset, false);
				
				if (assembled == null) {
					if (codeLineParts.length > 1) {
						sb.append("         ");
					}
					
					// This is considered a label and there isn't code
					for (int codePart = 1; codePart < codeLineParts.length; codePart++) {
						sb.append("//").append(codeLineParts[codePart]);
					}
					
					if (codeLineParts.length > 1) {
						sb.append("\n");
					}
					
					asmOffset++;
				} else if (assembled.startsWith("//")) {
					// If the assembler put in a comment (for instance with a branch label)
					if (textFormatChooser.getSelectedIndex() == 1) {
						sb.append("         ");
					}
					
					sb.append("// ");
					sb.append(codeLine);
					sb.append("\n");
					
					asmOffset++;
				} else {
					switch(textFormatChooser.getSelectedIndex()) {
						case 0:
							sb.append(assembled).append("\n");
							break;
						case 1:
							// Add the line of code
							sb.append("         16'd").append(i - asmOffset).append(": out = 32'b").
									append(assembled).append("; // ").append(codeLineParts[0]);

							// Add any comment
							for (int codePart = 1; codePart < codeLineParts.length; codePart++) {
								sb.append("//").append(codeLineParts[codePart]);
							}

							sb.append("\n");
							break;
						case 2:
							if ((i - asmOffset) % 2 == 0) {
								sb.append(NOP).append(assembled).append("\n");
							} else {
								int index = sb.lastIndexOf(NOP);
								sb.replace(index, index + 32, assembled);
							}
							break;
					}
				}
			}
		}
		
		// Rom case
		if (textFormatChooser.getSelectedIndex() == 1) {
			sb.append("         default: out = 32'b11010110000000000000001111100000; // BR XZR\n");
			sb.append("      endcase\n");
			sb.append("   end\n");
			sb.append("endmodule\n");
		} else {
			int index = sb.lastIndexOf("\n");
			if (index != -1) {
				sb.deleteCharAt(index);
			}
		}
		
		return sb.toString();
	}
	
	private class CodeDocumentListener implements DocumentListener {

		@Override
		public void changedUpdate(DocumentEvent arg0) {
			// Reassemble
			rightTextArea.setText(genLeftText());
		}

		@Override
		public void insertUpdate(DocumentEvent arg0) {
			// Reassemble
			rightTextArea.setText(genLeftText());
		}

		@Override
		public void removeUpdate(DocumentEvent arg0) {
			// Reassemble
			rightTextArea.setText(genLeftText());
		}
		
	}
	
	private class ReassembleActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// Reassemble
			reverseInputOutputOrder.setEnabled(textFormatChooser.getSelectedIndex() == 1);
			rightTextArea.setText(genLeftText());
		}
		
	}

	private class genFileActionListener implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			JFileChooser fileChooser = new JFileChooser();
			String fileName = fileNameField.getText();
			switch(textFormatChooser.getSelectedIndex()) {
				case 0:
					fileName += ".txt";
					break;
				case 1:
					fileName += ".v";
					break;
				case 2:
					fileName += ".mem";
					break;
			}
			fileChooser.setSelectedFile(new File(fileName));
			if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
				Writer writer = null;
				try {
					writer = new FileWriter(fileChooser.getSelectedFile());
					writer.write(genLeftText());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				} finally {
					try {
						if (writer != null) {
							writer.close();
						}
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null, e.getMessage());
					}
				}
			}
		}
		
	}
	
}
